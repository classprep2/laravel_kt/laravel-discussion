<?php

namespace App\Http\Controllers;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function create()
       {
           return view('posts.create');
       }
}
